///@headerfile
///@brief header file which contains required data and structures for pe files
#ifndef ASSIGNMENT_1_PE_FILE_READER_PE_DATA_H
#define ASSIGNMENT_1_PE_FILE_READER_PE_DATA_H

#include <inttypes.h>

#define IMAGE_SIZE_OF_SHORT_NAME 8


///@typedef
///@brief define BYTE type for best practice
typedef uint8_t BYTE;
///@typedef
///@brief define WORD type for best practice
typedef uint16_t WORD;
///@typedef
///@brief define DWORD type for best practice
typedef uint32_t DWORD;
typedef int32_t LONG;
///@struct
///@brief the structs contains necessary data for image section header of pe file
#ifdef _MSC_VER
#pragma pack(push, 1)
#endif
typedef struct
#ifdef __clang__
    __attribute__ ((__packed__))
#elif defined __GNUC__
        __attribute__ ((packed))
#endif
IMAGE_SECTION_HEADER {
    BYTE Name[IMAGE_SIZE_OF_SHORT_NAME];
    DWORD SizeOfRawData;
    LONG PointerToRawData;
} IMAGE_SECTION_HEADER;
#ifdef _MSC_VER
#pragma pack(pop)
#endif
///@struct
///@brief the structs contains necessary data for image dos header of pe file
#ifdef _MSC_VER
#pragma pack(push, 1)
#endif
typedef struct
#ifdef __clang__
    __attribute__ ((__packed__))
#elif defined __GNUC__
        __attribute__ ((__packed__))
#endif
IMAGE_DOS_HEADER {      // DOS .EXE header
    LONG e_magic;       // Magic number
    LONG e_lfanew;      // File address of new exe header
} IMAGE_DOS_HEADER;
#ifdef _MSC_VER
#pragma pack(pop)
#endif

///@struct
///@brief the structs contains necessary data for image file header of pe file
#ifdef _MSC_VER
#pragma pack(push, 1)
#endif
typedef struct
#ifdef __clang__
    __attribute__ ((__packed__))
#elif defined __GNUC__
        __attribute__ ((__packed__))
#endif
IMAGE_FILE_HEADER {
    WORD NumberOfSections;
} IMAGE_FILE_HEADER;
#ifdef _MSC_VER
#pragma pack(pop)
#endif
///@struct
///@brief the structs contains necessary data for image nt headers of pe file
#ifdef _MSC_VER
#pragma pack(push, 1)
#endif
typedef struct
#ifdef __clang__
    __attribute__ ((__packed__))
#elif defined __GNUC__
        __attribute__ ((__packed__))
#endif
IMAGE_NT_HEADERS64 {
    DWORD Signature;
    IMAGE_FILE_HEADER FileHeader;
} IMAGE_NT_HEADERS64;
#ifdef _MSC_VER
#pragma pack(pop)
#endif
///@struct
///@brief the structs contains necessary data for offsets and sizes of pe file
#ifdef _MSC_VER
#pragma pack(push, 1)
#endif
typedef struct
#ifdef __clang__
    __attribute__ ((__packed__))
#elif defined __GNUC__
        __attribute__ ((__packed__))
#endif
PEFile {
    /// @name Offsets within file
    ///@{
    /// Offset to e_lfanew
    LONG offset_to_e_lfanew;
    /// Size of image nt headers
    LONG size_of_file_header;
    /// Size of image optional header for x32 and x64 systems
    LONG size_of_image_optional_header32;
    LONG size_of_image_optional_header64;
    ///@}
} PEFile;
#ifdef _MSC_VER
#pragma pack(pop)
#endif

#endif //ASSIGNMENT_1_PE_FILE_READER_PE_DATA_H
