/// @file
/// @brief Main application file

#include <offset_calculator.h>
#include <pe_worker.h>
#include <stdlib.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f) {
    fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char **argv) {
    (void) argc;
    (void) argv;  // supress 'unused parameters' warning
    usage(stdout);

    char *in_file_name = argv[1];
    char *section_name = argv[2];
    char *out_file_name = argv[3];

    PEFile *peFile = malloc(sizeof(PEFile));
    FILE *in_file = fopen(in_file_name, "rb");


    calculate_offset_of_PEFile(peFile);

    IMAGE_SECTION_HEADER *sectionHeader = read_pe_file(in_file, section_name, peFile);
    if (sectionHeader == NULL) {
        free(sectionHeader);
        free(peFile);
        fprintf(stderr, "Invalid input data");
        fclose(in_file);
        return 1;
    }

    FILE *out_file = fopen(out_file_name, "wb");
    write_output_data(in_file, out_file, sectionHeader);

    fclose(in_file);
    free(sectionHeader);
    free(peFile);
    fclose(out_file);
    return 0;
}
