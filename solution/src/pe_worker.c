/// @file
/// @brief PE worker application file

/// standard valid value for e_magic of the image dos header
#define E_MAGIC_CONSTANT_x86 0x5a4d
/// standard valid value for start of the PE File data
#define PE_SIGNATURE 0x4550

#include <pe_worker.h>
#include <stdlib.h>


IMAGE_SECTION_HEADER *read_pe_file(FILE *in_file, char section_name[IMAGE_SIZE_OF_SHORT_NAME], PEFile *peFile) {
    // Структура заголовка DOS-образного файла
    IMAGE_DOS_HEADER dos_header = {0};
    // Считываем значения e_magic поля структуры
    fread(&dos_header.e_magic, sizeof(WORD), 1, in_file);
    fprintf(stdout, "INFO: current e_magic:%d\n", dos_header.e_magic);
    // Проверяем соответствие e_magic ожидаемому значению
    if (dos_header.e_magic != E_MAGIC_CONSTANT_x86) {
        fprintf(stderr, "Wrong e_magic\n");
        return NULL;
    }
    // Перемещаем указатель файла на смещение до начала PE-заголовка
    fseek(in_file, peFile->offset_to_e_lfanew, SEEK_SET);
    // Считываем значение смещения до начала PE-заголовка
    LONG tmp = 0;
    fread(&tmp, sizeof(LONG), 1, in_file);
    fprintf(stdout, "INFO: current offset to e_lfanew:%d\n", peFile->offset_to_e_lfanew);
    // Выравниваем смещение до начала PE-заголовка по DWORD
    dos_header.e_lfanew = tmp;
    fprintf(stdout, "INFO: current e_lfanew:%d\n", dos_header.e_lfanew);
    if (dos_header.e_lfanew % sizeof(DWORD) != 0) {
        fprintf(stderr, "Wrong e_lfanew\n");
        return NULL;
    }
    // Перемещаем указатель файла на начало PE-заголовка
    fseek(in_file, dos_header.e_lfanew, SEEK_SET);
    // Структура заголовка PE-образного файла
    IMAGE_NT_HEADERS64 imageNtHeaders64 = {0};
    // Считываем сигнатуру PE-заголовка
    fread(&imageNtHeaders64.Signature, sizeof(DWORD), 1, in_file);
    // Проверяем соответствие сигнатуры ожидаемому значению
    fprintf(stdout, "INFO: current signature:%u\n", imageNtHeaders64.Signature);
    if (imageNtHeaders64.Signature != PE_SIGNATURE) {
        fprintf(stderr, "Wrong signature\n");
        return NULL;
    }
    // Перемещаем указатель файла на количество секций в заголовке PE-файла
    fseek(in_file, sizeof(WORD), SEEK_CUR);
    fread(&imageNtHeaders64.FileHeader.NumberOfSections, sizeof(WORD), 1, in_file);
    // Перемещаем указатель файла на конец заголовка PE-файла
    fseek(in_file, peFile->size_of_file_header, SEEK_CUR);
    // Считываем значение поля Magic в заголовке PE-файла
    WORD optionalHeaderMagic = {0};

    fread(&optionalHeaderMagic, sizeof(WORD), 1,
          in_file);
    // читаем из файла значение optionalHeaderMagic размером в 2 байта
    LONG first_section_ptr = dos_header.e_lfanew;
    // получаем позицию начала PE-заголовка
    fprintf(stdout, "INFO: current optional header magic:%hu\n",
            optionalHeaderMagic);
    // выводим значение optionalHeaderMagic в консоль
    if (optionalHeaderMagic == 0x20B) {
        // если значение optionalHeaderMagic соответствует 0x20B
        first_section_ptr += peFile->size_of_image_optional_header64;
        // получаем позицию начала первой секции в файле, используя размер опционального заголовка PE-заголовка 64-битной версии
    }
    if (optionalHeaderMagic == 0x10B) {
        // если значение optionalHeaderMagic соответствует 0x10B
        first_section_ptr += peFile->size_of_image_optional_header32;
        // получаем позицию начала первой секции в файле, используя размер опционального заголовка PE-заголовка 32-битной версии
    }
    fprintf(stdout, "INFO: first section ptr:%d\n",
            first_section_ptr); // выводим позицию начала первой секции в консоль
    IMAGE_SECTION_HEADER *sectionHeader = malloc(
            sizeof(IMAGE_SECTION_HEADER)); // выделяем память под переменную sectionHeader типа IMAGE_SECTION_HEADER
    fseek(in_file, first_section_ptr, SEEK_SET); // перемещаем указатель файла на позицию начала первой секции
    for (WORD i = 0; i < imageNtHeaders64.FileHeader.NumberOfSections; i++) { // проходим циклом по всем секциям в файле
        fread(sectionHeader->Name, sizeof(sectionHeader->Name), 1, in_file); // читаем имя секции из файла
        fseek(in_file, sizeof(DWORD) * 2, SEEK_CUR); // перемещаем указатель на 8 байт вперед, пропуская два DWORDа
        fread(&sectionHeader->SizeOfRawData, sizeof(sectionHeader->SizeOfRawData), 1, in_file); // читаем размер секции
        fread(&sectionHeader->PointerToRawData, sizeof(sectionHeader->PointerToRawData), 1,
              in_file); // читаем позицию начала секции
        if (memcmp(section_name, sectionHeader->Name, 6) == 0) { // если имя текущей секции соответствует искомому имени
            return sectionHeader; // возвращаем найденную секцию
        }
        fseek(in_file, sizeof(DWORD) * 3 + sizeof(WORD) * 2,
              SEEK_CUR); // перемещаем указатель на 20 байт вперед, пропуская 3 DWORDа и 2 WORDа
    }
    free(sectionHeader); // освобождаем память, выделенную под переменную sectionHeader
    fprintf(stderr, "Section not found\n"); // выводим сообщение об ошибке в стандартный поток ошибок
    return NULL; // возвращаем пустой указатель
}

void write_output_data(FILE *in_file, FILE *out_file, IMAGE_SECTION_HEADER *sectionHeader) {
// Перемещение указателя файла на начало секции входного файла
    fseek(in_file, sectionHeader->PointerToRawData, SEEK_SET);
// Выделение буфера для чтения данных из входного файла
    char *buff = malloc(sectionHeader->SizeOfRawData);
// Чтение данных из входного файла в буфер
    fread(buff, 1, sectionHeader->SizeOfRawData, in_file);
// Запись данных из буфера в выходной файл
    fwrite(buff, 1, sectionHeader->SizeOfRawData, out_file);
// Освобождение памяти, выделенной для буфера
    free(buff);
}

