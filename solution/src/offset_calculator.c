/// @file
/// @brief File for auto calculation offsets
#include <offset_calculator.h>

/// Size of file header the header of PE file
#define SIZE_OF_FILE_HEADER 20
/// Size of optional header in 32 bits machines of the header of PE file
#define SIZE_OF_OPTIONAL_HEADER32 248
/// Size of optional header in 64 bits machines of the header of PE file
#define SIZE_OF_OPTIONAL_HEADER64 264


void calculate_offset_of_PEFile(PEFile *file) {

// Здесь объявляется функция calculate_offset_of_PEFile, которая принимает указатель на структуру PEFile в качестве аргумента.

    file->offset_to_e_lfanew = 60;

// Здесь устанавливается значение поля offset_to_e_lfanew структуры PEFile, которое соответствует смещению от начала файла до поля e_lfanew заголовка PE-файла. Это смещение для PE-файлов всегда равно 60 байтам.

    file->size_of_file_header = SIZE_OF_FILE_HEADER - sizeof(WORD)*2;

// Здесь вычисляется значение поля size_of_file_header структуры PEFile, которое соответствует размеру заголовка файла в байтах. Размер заголовка файла равен константе SIZE_OF_FILE_HEADER, но из него вычитаются 4 байта, которые занимают два поля WORD в структуре IMAGE_FILE_HEADER заголовка PE-файла.

    file->size_of_image_optional_header32 = SIZE_OF_OPTIONAL_HEADER32;

// Здесь устанавливается значение поля size_of_image_optional_header32 структуры PEFile, которое соответствует размеру необязательного заголовка для 32-битных машин в PE-формате. Размер этого заголовка равен константе SIZE_OF_OPTIONAL_HEADER32.

    file->size_of_image_optional_header64 = SIZE_OF_OPTIONAL_HEADER64;

// Здесь устанавливается значение поля size_of_image_optional_header64 структуры PEFile, которое соответствует размеру необязательного заголовка для 64-битных машин в PE-формате. Размер этого заголовка равен константе SIZE_OF_OPTIONAL_HEADER64.
}


